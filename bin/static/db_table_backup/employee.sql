-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2016 at 03:37 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `employee_performance_appraisal`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(15) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `team` varchar(50) DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_code`, `password`, `first_name`, `last_name`, `designation`, `team`, `is_admin`, `is_active`) VALUES
(1, 'CC001', 'welcome123', 'Srinivasan', 'Ganesan', 'Project Manager', 'Admin', 1, 1),
(2, 'CC002', 'welcome123', 'Vinoth', 'Sudaroli', 'Project Manager', 'Admin', 0, 1),
(3, 'CC003', 'welcome123', 'Selvin Durai', 'Thamodharan', 'Team Lead', 'IOS', 0, 1),
(4, 'CC004', 'welcome123', 'Manikandan', 'Selvam', 'Sr.Programmer', 'PHP', 0, 1),
(5, 'CC005', 'welcome123', 'Eswaran', 'Arumugam', 'Sr.Programmer', 'PHP', 0, 1),
(6, 'CC006', 'welcome123', 'Rajasekar', 'Periyasamy', 'Team Lead', 'PHP', 0, 1),
(7, 'CC007', 'welcome123', 'Prakashkumar', 'Subramaniam', 'Sr.Programmer', 'PHP', 0, 1),
(8, 'CC010', 'welcome123', 'Santhosh Nirmal', 'George Joseph Raj', 'Quality Analyst', 'Testing', 0, 1),
(9, 'CC011', 'welcome123', 'Jagadish', 'Sellamuthu', 'Sr.Programmer', 'Android', 0, 1),
(10, 'CC012', 'welcome123', 'Ganeshkumar', 'Palanisamy', 'Quality Analyst', 'Testing', 0, 1),
(11, 'CC015', 'welcome123', 'Arulmozhi Varman', 'Govindarajan', 'Business development Manager', 'Project Management', 0, 1),
(12, 'CC016', 'welcome123', 'Vijayalakshmi', 'Kanagarajan', 'Programmer', 'IOS', 0, 1),
(13, 'CC017', 'welcome123', 'Manoj Prabhu', 'Thangavel', 'Programmer', '.Net', 0, 1),
(14, 'CC022', 'welcome123', 'Jothimanickam', 'Muruganantham', 'Programmer', 'Java', 0, 1),
(15, 'CC023', 'welcome123', 'Dhivahar', 'Meenakshi Sundaram', 'User Interface Developer', 'Design', 0, 1),
(16, 'CC024', 'welcome123', 'Ezhumalai', 'DatchanaMoorthy', 'Sr.Programmer', 'Java', 0, 1),
(17, 'CC027', 'welcome123', 'Kabildev', 'Chellamuthu', 'Programmer', 'Java', 0, 1),
(18, 'CC028', 'welcome123', 'Venugopal', 'Chandarasekar', 'Programmer', '.Net', 0, 1),
(19, 'CC029', 'welcome123', 'Karthi', 'Nalliyappan', 'Programmer', 'IOS', 0, 1),
(20, 'CC030', 'welcome123', 'Subashree', 'Somasundaram', 'Programmer', 'IOS', 0, 1),
(21, 'CC031', 'welcome123', 'Aashik Hameed', 'Abdul Jaleel', 'Programmer', 'Android', 0, 1),
(22, 'CC033', 'welcome123', 'Rajesh', 'Dharmalingam', 'Programmer', 'Java', 0, 1),
(23, 'CC034', 'welcome123', 'Ananda Raj', 'Joseph', 'Accountant', 'Admin', 0, 1),
(24, 'CC036', 'welcome123', 'Sri Ram', 'Sudhakar', 'Senior Programmer', 'Java', 0, 1),
(25, 'CC037', 'welcome123', 'Yogapraba', 'Subramanian', 'Business Analyst', 'Project Management', 0, 1),
(26, 'CC039', 'welcome123', 'Vasanth T S', 'Santharam', 'trainee', 'PHP', 0, 1),
(27, 'CC040', 'welcome123', 'Renuga', 'Ponnusamy', 'HR', 'Admin', 1, 1),
(28, 'CC041', 'welcome123', 'Gowri Sankari', 'Seerangan', 'Trainee', 'iOS', 0, 1),
(29, 'CC042', 'welcome123', 'Priyanga', 'Mahendiran', 'Trainee', 'iOS', 0, 1),
(30, 'CC058', 'welcome123', 'Nandhini', 'Kumar', 'Programmer', 'UI Design', 0, 1),
(31, 'CC060', 'welcome123', 'Karuppasamy', 'Mariappan', 'Programmer', 'Java', 0, 1),
(32, 'CC062', 'welcome123', 'Vinoth Kumar', 'Subramanian', 'Trainee', '.Net', 0, 1),
(33, 'CC063', 'welcome123', 'Bharathi Raja', 'Selvakumarasamy', 'Trainee', 'Java', 0, 1),
(34, 'CC070', 'welcome123', 'Sathiyaseeelan', 'Govindarajan', 'System Admin', 'Admin', 0, 1),
(35, 'CC071', 'welcome123', 'Ganesh Kumar', 'Balasundaram', 'Business Analyst', 'Project Management', 0, 1),
(36, 'CC072', 'welcome123', 'Yogapriya', 'Mohanasundaram', 'Designer', 'UI Team', 0, 1),
(37, 'CC073', 'welcome123', 'Edward Sagayaraj', 'Christuraj', 'Sr.Programmer', 'IOS', 0, 1),
(38, 'CC074', 'welcome123', 'Arockia Raj', 'Laser', 'Sr.Programmer', '.Net', 0, 1),
(39, 'FY007', 'welcome123', 'Kavi priya', 'Sekar', 'Quality Analyst', 'QA', 0, 1),
(40, 'FY008', 'welcome123', 'Dinesh', 'Paneer Selvam', 'Sr.Programmer', 'PHP', 0, 1),
(41, 'FY009', 'welcome123', 'Shiva sankar', 'Paramaguru', 'Programmer', 'Android', 0, 1),
(42, 'FY013', 'welcome123', 'Karthikeyan', 'Thirugnana Sambandam A', 'Programmer', 'iOS', 0, 1),
(43, 'FY032', 'welcome123', 'Arockia samy', 'Anthony', 'Programmer', 'iOS', 0, 1),
(44, 'FY029', 'welcome123', 'Karthik', 'Sundaram', 'Sr.Programmer', 'iOS', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

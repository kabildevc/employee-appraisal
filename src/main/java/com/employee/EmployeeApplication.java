package com.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@SpringBootApplication
@Controller
public class EmployeeApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(EmployeeApplication.class, args);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String loginHome() throws Exception {
		return "redirect:/employee/login";
	}
	
//	@RequestMapping(method = RequestMethod.GET, value = "/**")
//	public String error() throws Exception {
//		return "redirect:/employee/login";
//	}
}

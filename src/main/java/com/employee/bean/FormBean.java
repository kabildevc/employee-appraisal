/**
 * 
 */
package com.employee.bean;

import java.util.List;

/**
 * @author admin
 *
 */
public class FormBean {

	private List<QuestionBean> questions;
	private int apprisalId;
	private int isSubmitted;
	private String notes;
	private int empApprisalAnsId;

	/**
	 * @return the questions
	 */
	public List<QuestionBean> getQuestions() {
		return questions;
	}
	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<QuestionBean> questions) {
		this.questions = questions;
	}
	/**
	 * @return the apprisalId
	 */
	public int getApprisalId() {
		return apprisalId;
	}
	/**
	 * @param apprisalId the apprisalId to set
	 */
	public void setApprisalId(int apprisalId) {
		this.apprisalId = apprisalId;
	}
	/**
	 * @return the isSubmitted
	 */
	public int getIsSubmitted() {
		return isSubmitted;
	}
	/**
	 * @param isSubmitted the isSubmitted to set
	 */
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the empApprisalAnsId
	 */
	public int getEmpApprisalAnsId() {
		return empApprisalAnsId;
	}
	/**
	 * @param empApprisalAnsId the empApprisalAnsId to set
	 */
	public void setEmpApprisalAnsId(int empApprisalAnsId) {
		this.empApprisalAnsId = empApprisalAnsId;
	}

}

/**
 * 
 */
package com.employee.bean;

/**
 * @author admin
 *
 */
public class QuestionBean {

	private int qusId;
	private int ansId;
	private int isChecked;
	private String justification;
	private int employeeWeightage;
	private int reviewerWeightage;
	private int appraiserWeightage;
	private String remarks;
	/**
	 * @return the qusId
	 */
	public int getQusId() {
		return qusId;
	}
	/**
	 * @param qusId the qusId to set
	 */
	public void setQusId(int qusId) {
		this.qusId = qusId;
	}
	/**
	 * @return the ansId
	 */
	public int getAnsId() {
		return ansId;
	}
	/**
	 * @param ansId the ansId to set
	 */
	public void setAnsId(int ansId) {
		this.ansId = ansId;
	}
	/**
	 * @return the isChecked
	 */
	public int getIsChecked() {
		return isChecked;
	}
	/**
	 * @param isChecked the isChecked to set
	 */
	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}
	/**
	 * @return the justification
	 */
	public String getJustification() {
		return justification;
	}
	/**
	 * @param justification the justification to set
	 */
	public void setJustification(String justification) {
		this.justification = justification;
	}
	/**
	 * @return the employeeWeightage
	 */
	public int getEmployeeWeightage() {
		return employeeWeightage;
	}
	/**
	 * @param employeeWeightage the employeeWeightage to set
	 */
	public void setEmployeeWeightage(int employeeWeightage) {
		this.employeeWeightage = employeeWeightage;
	}
	/**
	 * @return the reviewerWeightage
	 */
	public int getReviewerWeightage() {
		return reviewerWeightage;
	}
	/**
	 * @param reviewerWeightage the reviewerWeightage to set
	 */
	public void setReviewerWeightage(int reviewerWeightage) {
		this.reviewerWeightage = reviewerWeightage;
	}
	/**
	 * @return the appraiserWeightage
	 */
	public int getAppraiserWeightage() {
		return appraiserWeightage;
	}
	/**
	 * @param appraiserWeightage the appraiserWeightage to set
	 */
	public void setAppraiserWeightage(int appraiserWeightage) {
		this.appraiserWeightage = appraiserWeightage;
	}
	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}
	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}

/**
 * @author bharathiraja & Kabildev
 * @version since 1.0
 * @company Concertcare
 */
package com.employee.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.employee.dao.AdminDao;
import com.employee.entity.Employee;
import com.employee.util.Util;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	AdminDao adminDao;

	@Autowired
	private Util util;

	@RequestMapping(method = RequestMethod.GET, value = "/dashboard")
	public ModelAndView dashboard(HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = ADMIN DASHBOARD :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			model = util.getSessionValue(session);
			if (model != null) {
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			if(employee.getIsAdmin() != 1){
				session.invalidate();
				model = util.getSessionValue(session);
				if (model != null) {
					return model;
				}
			}
			response.put("employee", employee);
			response = adminDao.getSubmitedForms();
			if (response.get("error") != null || response == null) {
				model = new ModelAndView("/common/login");
				model.addObject("response", response);
				return model;
			}
			model = new ModelAndView("/admin/dashboard");
			model.addObject("response", response);

		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = ADMIN DASHBOARD :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = ADMIN DASHBOARD :::::");
		return model;
	}

	@RequestMapping(value = "/viewEmployeeForm", method = RequestMethod.GET)
	public ModelAndView getFormByEmployeeId(HttpSession session, int empId) throws Exception {
		logger.info("::::: Enter==>METHOD = GET EMPLOYEE FORM :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			String error = (String) session.getAttribute("error");
			String success = (String) session.getAttribute("success");
			model = util.getSessionValue(session);
			if (model != null) {
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			if(employee.getIsAdmin() != 1){
				session.invalidate();
				model = util.getSessionValue(session);
				if (model != null) {
					return model;
				}
			}
			response.put("employee", employee);
			if (empId > 0) {
				response = adminDao.getFormByEmployeeId(empId);
				if (error == null) {
					response.put("error", null);
				} else {
					response.put("error", error);
					session.setAttribute("error", null);
				}
				if (success == null) {
					response.put("success", null);
				} else {
					response.put("success", success);
					session.setAttribute("success", null);
				}
				if (response.get("error") != null) {
					model = new ModelAndView("/common/login");
					model.addObject("response", response);
					return model;
				}
				model = new ModelAndView("/admin/admin_form");
				model.addObject("response", response);
				return model;
			} else {
				response.put("error", "Something Went Wrong");
				model = new ModelAndView("redirect:/admin/dashboard");
				model.addObject("response", response);
				return model;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("::::: Exception==>METHOD = GET EMPLOYEE FORM :::::");
		}
		logger.info("::::: Exit==>METHOD = GET EMPLOYEE FORM :::::");
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveForm")
	public ModelAndView saveForm(Object formData, HttpSession session, HttpServletRequest req) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			model = util.getSessionValue(session);
			if (model != null) {
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			response = adminDao.saveForm(req, employee);
			if (response.get("error") != null && response.get("empId") != null) {
				model = new ModelAndView("redirect:admin/viewEmployeeForm?empId=" + response.get("empId"));
				session.setAttribute("error", response.get("error"));
				return model;
			}
			if (response.get("error") != null) {
				model = new ModelAndView("redirect:admin/dashboard");
				session.setAttribute("error", response.get("error"));
				return model;
			}
			if (response.get("empId") != null) {
				model = new ModelAndView("redirect:/admin/viewEmployeeForm?empId=" + response.get("empId"));
				session.setAttribute("success", response.get("success"));
				return model;
			}

		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
}

/**
 * @author bharathiraja & Kabildev
 * @version since 1.0
 * @company Concertcare
 */
package com.employee.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.employee.bean.LoginBean;
import com.employee.bean.PasswordBean;
import com.employee.dao.EmployeeDao;
import com.employee.entity.Employee;
import com.employee.util.Util;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private Util util;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String loginHome() throws Exception {
		logger.info("::::: Enter==>METHOD = LOGIN :::::");
		return "redirect:/employee/login";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/login")
	public ModelAndView login(@ModelAttribute LoginBean loginBean,HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = LOGIN :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		String error = null;
		try {
			//session.setAttribute("employee", null);
			loginBean = (LoginBean) session.getAttribute("loginBean");
			if(loginBean == null){
				loginBean = new LoginBean();
				response.put("loginBean", loginBean);
			}else{
				response.put("loginBean", loginBean);
				session.setAttribute("loginBean", null);
			}
			error = (String) session.getAttribute("error");
			if(error == null){
				response.put("error", null);
			}else{
				response.put("error", error);
				session.setAttribute("error", null);
			}
			
			model =new ModelAndView("common/login");
			model.addObject("response", response);
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = LOGIN :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = LOGIN :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/userAuthentication")
	public ModelAndView userAuthentication(@ModelAttribute LoginBean loginBean,HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			response = employeeDao.userAuthentication(loginBean);
			if (response.get("error") != null) {
				model =new ModelAndView("redirect:/employee/login");
				session.setAttribute("error", response.get("error"));
				session.setAttribute("loginBean", loginBean);
				return model;
			}
			if(Integer.parseInt(response.get("isChangePassword").toString()) == 0){
				model =new ModelAndView("redirect:/employee/changePassword");
				session.setAttribute("employee", response.get("employee"));
				model.addObject("response", response);
				return model;
			}
			if(Integer.parseInt(response.get("isAdmin").toString()) ==1 ){
				model =new ModelAndView("redirect:/admin/dashboard");
				session.setAttribute("employee", response.get("employee"));
				return model;
			}else{
				model =new ModelAndView("redirect:/employee/dashboard");
				session.setAttribute("employee", response.get("employee"));
				return model;
			}
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/changePassword")
	public ModelAndView changePassword(HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			model = util.getSessionValue(session);
			if(model != null){
				return model;
			}
			model =new ModelAndView("common/change_password");
			PasswordBean passwordBean = new PasswordBean();
			response.put("passwordBean", passwordBean);
			response.put("error", null);
			model.addObject("response", response);
			return model;
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/changePassword")
	public ModelAndView changePassword(@ModelAttribute PasswordBean passwordBean,HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		LoginBean loginBean = new LoginBean();
		try {
			model = util.getSessionValue(session);
			if(model != null){
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			if(!passwordBean.getConfirmPassword().equals(passwordBean.getNewPassword())){
				model =new ModelAndView("redirect:/employee/changePassword");
				response.put("error", "UserName or Password Invalid !");
				response.put("passwordBean", passwordBean);
				model.addObject("response", response);
				return model;
			}
			response = employeeDao.changePassword(passwordBean,employee);
			if (response.get("error") != null) {
				model =new ModelAndView("common/login");
				response.put("loginBean", loginBean);
				model.addObject("response", response);
				return model;
			}
			if(Integer.parseInt(response.get("isAdmin").toString()) ==1 ){
				model =new ModelAndView("redirect:/admin/dashboard");
				model.addObject("response", response);
				return model;
			}else{
				model =new ModelAndView("redirect:/employee/dashboard");
				model.addObject("response", response);
				return model;
			}
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/dashboard")
	public ModelAndView dashboard(HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			String error = (String) session.getAttribute("error");
			String success = (String) session.getAttribute("success");
			String message = (String) session.getAttribute("message");
			
			model = util.getSessionValue(session);
			if(model != null){
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			response = employeeDao.getQuestionAndSavedAnswer(employee);
			if(error == null){
				response.put("error", null);
			}else{
				response.put("error", error);
				session.setAttribute("error" , null);
			}
			if(success == null){
				response.put("success", null);
			}else{
				response.put("success", success);
				session.setAttribute("success" , null);
			}
			if(message == null){
				response.put("message", null);
			}else{
				response.put("message", message);
				session.setAttribute("message" , null);
			}
			model =new ModelAndView("employee/dashboard");
			model.addObject("response", response);
			return model;
			
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/logout")
	public ModelAndView logout(HttpSession session) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			session.invalidate();
			System.out.println(session);
			model =new ModelAndView("redirect:/employee/login");
			model.addObject("response", response);
			return model;
			
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveForm")
	public ModelAndView saveForm(Object formData,HttpSession session, HttpServletRequest req) throws Exception {
		logger.info("::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		ModelAndView model = null;
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			model = util.getSessionValue(session);
			if(model != null){
				return model;
			}
			Employee employee = (Employee) session.getAttribute("employee");
			response = employeeDao.saveForm(req,employee);
			if (response.get("error") != null) {
				model =new ModelAndView("redirect:/employee/dashboard");
				session.setAttribute("error", response.get("error"));
				return model;
			}
			model =new ModelAndView("redirect:/employee/dashboard");
			session.setAttribute("success", response.get("success"));
			session.setAttribute("message", response.get("message"));
			return model;
			
		} catch (Exception e) {
			logger.info("::::: Exception==>METHOD = USER AUTHENTICATION :::::" + e);
		}
		logger.info("::::: Exit==>METHOD = USER AUTHENTICATION :::::");
		return model;
	}
	
//	@ExceptionHandler(Throwable.class)
//	  public String handleAnyException(Throwable ex, HttpServletRequest request) {
//	    return ClassUtils.getShortName(ex.getClass());
//	  }
}

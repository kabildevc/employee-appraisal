package com.employee.dao;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.employee.entity.Employee;

public interface AdminDao {

	Map<String, Object> getFormByEmployeeId(int empId);

	Map<String, Object> getSubmitedForms();

	Map<String, Object> saveForm(HttpServletRequest req, Employee employee);

}

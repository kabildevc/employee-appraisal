package com.employee.dao;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.employee.bean.LoginBean;
import com.employee.bean.PasswordBean;
import com.employee.entity.Employee;

public interface EmployeeDao {

	Map<String, Object> userAuthentication(LoginBean loginBean);

	Map<String, Object> changePassword(PasswordBean passwordBean, Employee employee);

	Map<String, Object> getQuestionAndSavedAnswer(Employee employee);

	Map<String, Object> saveForm(HttpServletRequest req, Employee employee);
}

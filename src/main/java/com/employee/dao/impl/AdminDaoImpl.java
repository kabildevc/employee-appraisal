package com.employee.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dao.AdminDao;
import com.employee.entity.ApprisalQuestionSections;
import com.employee.entity.ApprisalQuestions;
import com.employee.entity.Apprisals;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeApprisalAnswer;
import com.employee.entity.EmployeeApprisalAnswerGroup;
import com.employee.service.AdminService;
import com.employee.service.EmployeeService;

@Service
public class AdminDaoImpl implements AdminDao {

	public static final Logger logger = LoggerFactory.getLogger(AdminDao.class);

	@Autowired
	AdminService adminService;

	@Autowired
	EmployeeService employeeService;

	@Override
	public Map<String, Object> getSubmitedForms() {
		logger.info(":::::: Enter==>IMPLEMENTATION METHOD = ADMIN DASHBOARD :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		List<Map<String, Object>> apprisalAnswerGroups = null;
		try {
			apprisalAnswerGroups = adminService.getSubmitedForms();
			if (apprisalAnswerGroups.size() > 0) {
				response.put("list", apprisalAnswerGroups);
			} else {
				response.put("list", null);
			}
		} catch (Exception e) {
			logger.info(":::::: Exception==>IMPLEMENTATION METHOD = ADMIN DASHBOARD :::::");
			e.printStackTrace();
		}
		logger.info(":::::: Exit==>IMPLEMENTATION METHOD = ADMIN DASHBOARD :::::");
		return response;
	}

	// @Override
	public Map<String, Object> getFormByEmpId(int empId) {
		logger.info(":::::: Enter==>IMPLEMENTATION METHOD = GET EMPLOYEE FORM :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		List<ApprisalQuestionSections> apprisalQuestionSections = null;
		Employee employee = null;
		List<Map<String, Object>> employeeForm = null;
		try {
			employee = adminService.getEmployeeById(empId);
			if (employee != null) {
				apprisalQuestionSections = adminService.getQuestionSection();
				if (apprisalQuestionSections != null) {
					response.put("questionSections", apprisalQuestionSections);
				}
				employeeForm = adminService.getEmployeeForm(empId);
				if (employeeForm.size() > 0) {
					response.put("employeeForm", employeeForm);
				}
			} else {
				response.put("error", "something went wrong");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("error", "something went wrong");
			logger.info(":::::: Exception==>IMPLEMENTATION METHOD = GET EMPLOYEE FORM :::::");
		}
		logger.info(":::::: Exit==>IMPLEMENTATION METHOD = GET EMPLOYEE FORM :::::");
		return response;
	}

	@Override
	public Map<String, Object> getFormByEmployeeId(int empId) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		List<ApprisalQuestionSections> apprisalQuestionSections = null;
		List<Object> questionSectionList = new ArrayList<Object>();
		Map<String, Object> questionSectionMap = null;
		List<Object> questionList = new ArrayList<Object>();
		Map<String, Object> questionMap = null;
		List<ApprisalQuestions> apprisalQuestions = null;
		EmployeeApprisalAnswer employeeApprisalAnswer = null;
		List<Apprisals> apprisal = null;
		Apprisals apprisals = null;
		EmployeeApprisalAnswerGroup answerGroup = null;
		Employee employee = null;
		int employeeWeightage = 0;
		int employeeTotalWeightage = 0;
		int reviewerWeightage = 0;
		int reviewerTotalWeightage = 0;
		int appraiserWeightage = 0;
		int appraiserTotalWeightage = 0;
		try {
			employee = employeeService.getEmployeeById(empId);
			if (employee != null) {
				response.put("employee", employee);
			}
			apprisal = employeeService.getApprisals(1, 0, 0);
			if(apprisal.size() > 0){
				apprisals= apprisal.get(0);
			}
			if (apprisals == null) {
				response.put("apprisalId", null);
			} else {
				response.put("apprisalId", apprisals.getApprisalId());
			}
			answerGroup = employeeService.getEmployeeApprisalAnswerGroupByEmployeeIdAndIsSubmitted(empId,1);
			if (answerGroup == null) {
				response.put("empApprisalAnsId", null);
				response.put("notes", null);
				response.put("isSubmitted", 0);
			} else {
				response.put("empApprisalAnsId", answerGroup.getEmpApprisalAnsId());
				response.put("notes", answerGroup.getNotes());
				response.put("isSubmitted", answerGroup.getIsSubmitted());
			}

			apprisalQuestionSections = employeeService.getApprisalQuestionSections(0);
			for (ApprisalQuestionSections apprisalQuestionSection : apprisalQuestionSections) {
				questionSectionMap = new HashMap<String, Object>();
				apprisalQuestions = employeeService.getApprisalQuestions(apprisalQuestionSection.getQusSectionId(), 0);
				questionList = new ArrayList<Object>();
				for (ApprisalQuestions apprisalQues : apprisalQuestions) {
					questionMap = new HashMap<String, Object>();
					questionMap.put("qusId", apprisalQues.getQusId());
					questionMap.put("question", apprisalQues.getQuestion());
					questionMap.put("weightage", apprisalQues.getWeightage());

					employeeApprisalAnswer = employeeService.getEmployeeApprisalAnswer(empId, apprisalQues.getQusId());

					String quesIsChecked = "quesIsChecked_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesAnswer = "quesAnswer_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesWeightAge = "quesWeightAge_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String reviewerWeightAge = "reviewerWeightAge_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String appriserWeightAge = "appriserWeightAge_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String remarks = "remarks" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String ansId = "ansId" + apprisalQuestionSection.getQusSectionId() + "_" + apprisalQues.getQusId();

					questionMap.put("quesIsChecked", quesIsChecked);
					questionMap.put("quesAnswer", quesAnswer);
					questionMap.put("quesWeightAge", quesWeightAge);
					questionMap.put("reviewerWeightAge", reviewerWeightAge);
					questionMap.put("appriserWeightAge", appriserWeightAge);
					questionMap.put("remarks", remarks);
					questionMap.put("ansId", ansId);
					
					if (employeeApprisalAnswer != null) {
						questionMap.put(quesIsChecked, employeeApprisalAnswer.getIsChecked());
						questionMap.put(quesAnswer, employeeApprisalAnswer.getJustification());
						questionMap.put(quesWeightAge, employeeApprisalAnswer.getEmployeeWeightage());
						questionMap.put(reviewerWeightAge, employeeApprisalAnswer.getReviewerWeightage());
						questionMap.put(appriserWeightAge, employeeApprisalAnswer.getAppraiserWeightage());
						questionMap.put(remarks, employeeApprisalAnswer.getRemarks());
						questionMap.put(ansId, employeeApprisalAnswer.getAnsId());
						if(employeeApprisalAnswer.getIsChecked() == 1){
							employeeWeightage = employeeWeightage + employeeApprisalAnswer.getEmployeeWeightage();
							employeeTotalWeightage = employeeTotalWeightage + apprisalQues.getWeightage();
						}
						if(employeeApprisalAnswer.getReviewerWeightage() > 0){
							reviewerWeightage = reviewerWeightage + employeeApprisalAnswer.getReviewerWeightage();
							reviewerTotalWeightage = reviewerTotalWeightage + apprisalQues.getWeightage();
						}
						if(employeeApprisalAnswer.getAppraiserWeightage() > 0){
							appraiserWeightage = appraiserWeightage + employeeApprisalAnswer.getAppraiserWeightage();
							appraiserTotalWeightage = appraiserTotalWeightage + apprisalQues.getWeightage();
						}
					} else {
						questionMap.put(quesIsChecked, 0);
						questionMap.put(quesAnswer, null);
						questionMap.put(quesWeightAge, 0);
						questionMap.put(reviewerWeightAge, 0);
						questionMap.put(appriserWeightAge, 0);
						questionMap.put(remarks, null);
						questionMap.put(ansId, 0);
					}

					questionList.add(questionMap);

				}
				questionSectionMap.put("qusSectionId", apprisalQuestionSection.getQusSectionId());
				questionSectionMap.put("sectionTitle", apprisalQuestionSection.getSectionTitle());
				questionSectionMap.put("isMandatory", apprisalQuestionSection.getIsMandatory());
				questionSectionMap.put("questions", questionList);
				questionSectionList.add(questionSectionMap);
			}
			response.put("questionSectionList", questionSectionList);
			if(appraiserTotalWeightage > 0){
				response.put("appraiserTotalWeightage", appraiserTotalWeightage);
				response.put("appraiserWeightage", appraiserWeightage);
			}else{
				response.put("appraiserTotalWeightage", 0);
				response.put("appraiserWeightage", 0);
			}
			if(reviewerTotalWeightage > 0){
				response.put("reviewerTotalWeightage", reviewerTotalWeightage);
				response.put("reviewerWeightage", reviewerWeightage);
			}else{
				response.put("reviewerTotalWeightage", 0);
				response.put("reviewerWeightage", 0);
			}
			if(employeeTotalWeightage > 0){
				response.put("employeeTotalWeightage", employeeTotalWeightage);
				response.put("employeeWeightage", employeeWeightage);
			}else{
				response.put("employeeTotalWeightage", 0);
				response.put("employeeWeightage", 0);
			}
		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Something went wrong Login Again!");
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}

	@Override
	public Map<String, Object> saveForm(HttpServletRequest req, Employee employee) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		EmployeeApprisalAnswerGroup employeeApprisalAnswerGroup = null;
		EmployeeApprisalAnswer employeeApprisalAnswer = null;
		List<ApprisalQuestionSections> apprisalQuestionSections = null;
		List<ApprisalQuestions> apprisalQuestion = null;
		try {
			String empApprisalAnsIdStr = req.getParameter("empApprisalAnsId");
			int empApprisalAnsId = 0;
			if (empApprisalAnsIdStr != null && empApprisalAnsIdStr.length() != 0) {
				empApprisalAnsId = Integer.parseInt(empApprisalAnsIdStr);
				employeeApprisalAnswerGroup = employeeService.getEmployeeApprisalAnswerGroupById(empApprisalAnsId);
				if (employeeApprisalAnswerGroup != null) {
					response.put("empId", employeeApprisalAnswerGroup.getEmpId());
					employeeApprisalAnswerGroup.setAppraiserId(employee.getEmpId());
					employeeApprisalAnswerGroup.setAppraiserUpdatedDate(employeeService.getUtcTime());
					String notes = req.getParameter("notes");
					if (notes != null) {
						employeeApprisalAnswerGroup.setNotes(notes);
					}
					employeeApprisalAnswerGroup = employeeService
							.saveEmployeeApprisalAnswerGroup(employeeApprisalAnswerGroup);
				}
			}

			apprisalQuestionSections = employeeService.getApprisalQuestionSections(0);
			for (ApprisalQuestionSections apprisalQuestionSection : apprisalQuestionSections) {
				apprisalQuestion = employeeService.getApprisalQuestions(apprisalQuestionSection.getQusSectionId(), 0);
				for (ApprisalQuestions apprisalQues : apprisalQuestion) {
					String reviewerWeightAge = "reviewerWeightAge_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String appriserWeightAge = "appriserWeightAge_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String remarks = "remarks" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String ansId = "ansId" + apprisalQuestionSection.getQusSectionId() + "_" + apprisalQues.getQusId();

					String reviewerWeightAgeStr = req.getParameter(reviewerWeightAge);
					String appriserWeightAgeStr = req.getParameter(appriserWeightAge);
					String remarksStr = req.getParameter(remarks);
					String ansIdStr = req.getParameter(ansId);
					int answerId = 0;
					if (ansIdStr != null && ansIdStr.length() > 0) {
						answerId = Integer.parseInt(ansIdStr);
					}
					if (answerId > 0) {
						employeeApprisalAnswer = adminService.getEmployeeAnswerById(answerId);
						if (employeeApprisalAnswer != null) {
							if (reviewerWeightAgeStr != null && reviewerWeightAgeStr.length() > 0) {
								employeeApprisalAnswer.setReviewerWeightage(Integer.parseInt(reviewerWeightAgeStr));
							}else{
								employeeApprisalAnswer.setReviewerWeightage(0);
							}
							if (appriserWeightAgeStr != null && appriserWeightAgeStr.length() > 0) {
								employeeApprisalAnswer.setAppraiserWeightage(Integer.parseInt(appriserWeightAgeStr));
							}else{
								employeeApprisalAnswer.setAppraiserWeightage(0);
							}
							if (remarksStr != null) {
								employeeApprisalAnswer.setRemarks(remarksStr);
							}
						}
						employeeApprisalAnswer = adminService.saveEmployeeApprisalAnswer(employeeApprisalAnswer);
					}
				}
			}
			response.put("success", "Successfully Saved");
			response.put("error", null);
		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Unable to save try again!");
			response.put("success", null);
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}

}

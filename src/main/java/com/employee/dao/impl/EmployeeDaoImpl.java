package com.employee.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.bean.FormBean;
import com.employee.bean.LoginBean;
import com.employee.bean.PasswordBean;
import com.employee.bean.QuestionBean;
import com.employee.dao.EmployeeDao;
import com.employee.entity.ApprisalQuestionSections;
import com.employee.entity.ApprisalQuestions;
import com.employee.entity.Apprisals;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeApprisalAnswer;
import com.employee.entity.EmployeeApprisalAnswerGroup;
import com.employee.service.EmployeeService;
import com.employee.util.Util;

@Service
public class EmployeeDaoImpl implements EmployeeDao {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImpl.class);

	@Autowired
	EmployeeService employeeService;

	@Override
	public Map<String, Object> userAuthentication(LoginBean loginBean) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		Employee employee = null;
		try {
			employee = employeeService.getEmployeeByEmpCode(loginBean.getEmpCode());
			if (employee == null) {
				response.put("error", "Username or Password Invalid!");
				return response;
			}
			if (!employee.getPassword().equals(Util.passwordEncryption(loginBean.getPassword()))) {
				response.put("error", "Username or Password Invalid!");
				return response;
			}
			response.put("empId", employee.getEmpId());
			response.put("isChangePassword", employee.getIsChangePassword());
			response.put("isAdmin", employee.getIsAdmin());
			response.put("employee", employee);

		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Something went wrong Login Again!");
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}

	@Override
	public Map<String, Object> changePassword(PasswordBean passwordBean, Employee employees) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		Employee employee = null;
		try {
			employee = employeeService.getEmployeeById(employees.getEmpId());
			if (employee == null) {
				response.put("error", "Something went wrong Login Again!");
				return response;
			}
			employee.setIsChangePassword(1);
			employee.setPassword(Util.passwordEncryption(passwordBean.getNewPassword()));
			employee = employeeService.saveEmployee(employee);

			response.put("isAdmin", employee.getIsAdmin());
			response.put("employee", employee);

		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Something went wrong Login Again!");
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}

	@Override
	public Map<String, Object> getQuestionAndSavedAnswer(Employee employee) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		List<ApprisalQuestionSections> apprisalQuestionSections = null;
		List<Object> questionSectionList = new ArrayList<Object>();
		Map<String, Object> questionSectionMap = null;
		List<Object> questionList = new ArrayList<Object>();
		Map<String, Object> questionMap = null;
		List<ApprisalQuestions> apprisalQuestions = null;
		EmployeeApprisalAnswer employeeApprisalAnswer = null;
		List<Apprisals> apprisal = null;
		Apprisals apprisals = null;
		EmployeeApprisalAnswerGroup answerGroup = null;
		try {
			apprisal = employeeService.getApprisals(1, 0, 0);
			if(apprisal.size() > 0){
				apprisals= apprisal.get(0);
			}
			if (apprisals == null) {
				response.put("apprisalId", null);
				response.put("apprisalTitle", null);
			} else {
				response.put("apprisalId", apprisals.getApprisalId());
				response.put("apprisalTitle", apprisals.getTitle());
			}
			answerGroup = employeeService.getEmployeeApprisalAnswerGroupByEmployeeId(employee.getEmpId());
			if (answerGroup == null) {
				response.put("empApprisalAnsId", null);
				response.put("isSubmitted", 0);
			} else {
				response.put("empApprisalAnsId", answerGroup.getEmpApprisalAnsId());
				response.put("isSubmitted", answerGroup.getIsSubmitted());
			}

			apprisalQuestionSections = employeeService.getApprisalQuestionSections(0);
			for (ApprisalQuestionSections apprisalQuestionSection : apprisalQuestionSections) {
				questionSectionMap = new HashMap<String, Object>();
				apprisalQuestions = employeeService.getApprisalQuestions(apprisalQuestionSection.getQusSectionId(), 0);
				questionList = new ArrayList<Object>();
				for (ApprisalQuestions apprisalQues : apprisalQuestions) {
					questionMap = new HashMap<String, Object>();
					questionMap.put("qusId", apprisalQues.getQusId());
					questionMap.put("question", apprisalQues.getQuestion());
					questionMap.put("weightage", apprisalQues.getWeightage());
					employeeApprisalAnswer = employeeService.getEmployeeApprisalAnswer(employee.getEmpId(),
							apprisalQues.getQusId());
					String quesIsChecked = "quesIsChecked_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesAnswer = "quesAnswer" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesWeightAge = "quesWeightAge" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String ansId = "ansId_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					questionMap.put("quesIsChecked", quesIsChecked);
					questionMap.put("quesAnswer", quesAnswer);
					questionMap.put("quesWeightAge", quesWeightAge);
					questionMap.put("ansId", ansId);
					if (employeeApprisalAnswer != null) {
						questionMap.put(ansId, employeeApprisalAnswer.getAnsId());
						questionMap.put(quesIsChecked, employeeApprisalAnswer.getIsChecked());
						questionMap.put(quesAnswer, employeeApprisalAnswer.getJustification());
						questionMap.put(quesWeightAge, employeeApprisalAnswer.getEmployeeWeightage());
					} else {
						questionMap.put(ansId, 0);
						questionMap.put(quesIsChecked, 0);
						questionMap.put(quesAnswer, null);
						questionMap.put(quesWeightAge, 0);
					}
					questionList.add(questionMap);
				}
				questionSectionMap.put("qusSectionId", apprisalQuestionSection.getQusSectionId());
				questionSectionMap.put("sectionTitle", apprisalQuestionSection.getSectionTitle());
				questionSectionMap.put("isMandatory", apprisalQuestionSection.getIsMandatory());
				questionSectionMap.put("questions", questionList);
				questionSectionList.add(questionSectionMap);
			}
			response.put("questionSectionList", questionSectionList);
		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Something went wrong Login Again!");
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}

	@Override
	public Map<String, Object> saveForm(HttpServletRequest req, Employee employee) {
		logger.info(":::::: Enter==>METHOD = USER AUTHENTICATION :::::");
		Map<String, Object> response = new HashMap<String, Object>();
		EmployeeApprisalAnswerGroup apprisalQuestions = null;
		EmployeeApprisalAnswer employeeApprisalAnswer = null;
		List<EmployeeApprisalAnswer> employeeApprisalAnswerList = new ArrayList<EmployeeApprisalAnswer>();
		List<ApprisalQuestionSections> apprisalQuestionSections = null;
		List<ApprisalQuestions> apprisalQuestion = null;
		FormBean formBean = new FormBean();
		List<QuestionBean> questionBeans = new ArrayList<QuestionBean>();
		QuestionBean questionBean = null;
		try {

			apprisalQuestionSections = employeeService.getApprisalQuestionSections(0);
			for (ApprisalQuestionSections apprisalQuestionSection : apprisalQuestionSections) {
				apprisalQuestion = employeeService.getApprisalQuestions(apprisalQuestionSection.getQusSectionId(), 0);
				for (ApprisalQuestions apprisalQues : apprisalQuestion) {
					questionBean = new QuestionBean();
					String quesIsChecked = "quesIsChecked_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesAnswer = "quesAnswer" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String quesWeightAge = "quesWeightAge" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					String ansId = "ansId_" + apprisalQuestionSection.getQusSectionId() + "_"
							+ apprisalQues.getQusId();
					
					String isChecked = req.getParameter(quesIsChecked);
					String quesAns = req.getParameter(quesAnswer);
					String quesWeight = req.getParameter(quesWeightAge);
					String ansIdStr = req.getParameter(ansId);
					questionBean = new QuestionBean();
					if (ansIdStr != null && ansIdStr.length() > 0) {
						questionBean.setAnsId(Integer.parseInt(ansIdStr));
					} else {
						questionBean.setAnsId(0);
					}
					if (isChecked != null && isChecked.equals("on")) {
						questionBean.setIsChecked(1);
					} else {
						questionBean.setIsChecked(0);
					}
					if (quesWeight != null && quesWeight.length() > 0) {
						questionBean.setEmployeeWeightage(Integer.parseInt(quesWeight));
					} else {
						questionBean.setEmployeeWeightage(0);
					}
					questionBean.setJustification(quesAns);
					questionBean.setQusId(apprisalQues.getQusId());
					questionBean.setRemarks(null);
					questionBean.setReviewerWeightage(0);
					questionBeans.add(questionBean);
				}
			}
			formBean.setQuestions(questionBeans);
			String apprisalIdStr = req.getParameter("apprisalId");
			if (apprisalIdStr == null || apprisalIdStr.length() == 0) {
				formBean.setApprisalId(0);
			} else {
				formBean.setApprisalId(Integer.parseInt(apprisalIdStr));
			}
			String empApprisalAnsIdStr = req.getParameter("empApprisalAnsId");
			int empApprisalAnsId = 0;
			if (empApprisalAnsIdStr == null || empApprisalAnsIdStr.length() == 0) {
				formBean.setEmpApprisalAnsId(0);
			} else {
				empApprisalAnsId = Integer.parseInt(empApprisalAnsIdStr);
				formBean.setEmpApprisalAnsId(empApprisalAnsId);
			}
			String isSubmittedStr = req.getParameter("isSubmitted");
			int isSubmitted = 0;
			if (isSubmittedStr == null || isSubmittedStr.length() == 0) {
				formBean.setEmpApprisalAnsId(0);
			} else {
				isSubmitted = Integer.parseInt(isSubmittedStr);
				formBean.setIsSubmitted(isSubmitted);
			}
			apprisalQuestions = employeeService.getEmployeeApprisalAnswerGroupById(formBean.getEmpApprisalAnsId());
			if(apprisalQuestions == null){
				apprisalQuestions = new EmployeeApprisalAnswerGroup();
			}
			apprisalQuestions.setAppraiserId(0);
			apprisalQuestions.setApprisalId(formBean.getApprisalId());
			apprisalQuestions.setEmpApprisalAnsId(formBean.getEmpApprisalAnsId());
			apprisalQuestions.setEmpId(employee.getEmpId());
			
			if (apprisalQuestions.getEmpApprisalAnsId() == 0) {
				apprisalQuestions.setInsertedDate(employeeService.getUtcTime());
			} else {
				apprisalQuestions.setUpdatedDate(employeeService.getUtcTime());
			}
			apprisalQuestions.setIsSubmitted(formBean.getIsSubmitted());
			apprisalQuestions.setNotes(null);
			apprisalQuestions.setAppraiserUpdatedDate(null);

			for (QuestionBean questionBeanss : formBean.getQuestions()) {
				employeeApprisalAnswer = new EmployeeApprisalAnswer();
				employeeApprisalAnswer.setAnsId(questionBeanss.getAnsId());
				// employeeApprisalAnswer.setAppraiserWeightage(questionBean.getAppraiserWeightage());
				employeeApprisalAnswer.setEmployeeApprisalAnswerGroup(apprisalQuestions);
				employeeApprisalAnswer.setEmployeeWeightage(questionBeanss.getEmployeeWeightage());
				employeeApprisalAnswer.setIsChecked(questionBeanss.getIsChecked());
				employeeApprisalAnswer.setJustification(questionBeanss.getJustification());
				employeeApprisalAnswer.setQusId(questionBeanss.getQusId());
				// employeeApprisalAnswer.setRemarks(questionBean.getRemarks());
				employeeApprisalAnswer.setReviewerWeightage(questionBeanss.getReviewerWeightage());
				employeeApprisalAnswerList.add(employeeApprisalAnswer);
				// employeeApprisalAnswer =
				// employeeService.saveEmployeeApprisalAnswer(employeeApprisalAnswer);
			}
			if (empApprisalAnsId > 0) {
//				employeeService.deleteByEmpApprisalAnsId(empApprisalAnsId);
			}
			apprisalQuestions.setEmployeeApprisalAnswers(employeeApprisalAnswerList);
			apprisalQuestions = employeeService.saveEmployeeApprisalAnswerGroup(apprisalQuestions);
			if(isSubmitted == 1){
				response.put("message", "Your form submitted successfully");
			}else{
				response.put("success", "Successfully Saved");
			}
			response.put("error", null);
		} catch (Exception e) {
			logger.info("::::: Exception==>Enter METHOD = USER AUTHENTICATION :::::", e);
			response.put("error", "Unable to save try again!");
			response.put("success", null);
		}
		logger.info("::::: Exit==>Enter METHOD = USER AUTHENTICATION :::::");
		return response;
	}
}
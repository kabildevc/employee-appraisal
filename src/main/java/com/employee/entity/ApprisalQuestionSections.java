package com.employee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "apprisal_question_sections")
public class ApprisalQuestionSections {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "qus_section_id")
	private int qusSectionId;

	@Column(name = "section_title", nullable = true)
	private String sectionTitle;
	
	@Column(name = "is_mandatory", nullable = false)
	private int isMandatory;

	@JsonIgnore
	@Column(name = "is_deleted", nullable = false)
	private int isDeleted;

	/**
	 * @return the qusSectionId
	 */
	public int getQusSectionId() {
		return qusSectionId;
	}

	/**
	 * @param qusSectionId the qusSectionId to set
	 */
	public void setQusSectionId(int qusSectionId) {
		this.qusSectionId = qusSectionId;
	}

	/**
	 * @return the sectionTitle
	 */
	public String getSectionTitle() {
		return sectionTitle;
	}

	/**
	 * @param sectionTitle the sectionTitle to set
	 */
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	/**
	 * @return the isMandatory
	 */
	public int getIsMandatory() {
		return isMandatory;
	}

	/**
	 * @param isMandatory the isMandatory to set
	 */
	public void setIsMandatory(int isMandatory) {
		this.isMandatory = isMandatory;
	}

	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
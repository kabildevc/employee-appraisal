package com.employee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "apprisal_questions")
public class ApprisalQuestions {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "qus_id")
	private int qusId;

	@Column(name = "qus_section_id", nullable = true)
	private int qusSectionId;

	@JsonIgnore
	@Column(name = "question", nullable = true)
	private String question;
	
	@Column(name = "weightage", nullable = true)
	private int weightage;
	
	@Column(name = "is_deleted", nullable = true)
	private int isDeleted;

	/**
	 * @return the qusId
	 */
	public int getQusId() {
		return qusId;
	}

	/**
	 * @param qusId the qusId to set
	 */
	public void setQusId(int qusId) {
		this.qusId = qusId;
	}

	/**
	 * @return the qusSectionId
	 */
	public int getQusSectionId() {
		return qusSectionId;
	}

	/**
	 * @param qusSectionId the qusSectionId to set
	 */
	public void setQusSectionId(int qusSectionId) {
		this.qusSectionId = qusSectionId;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the weightage
	 */
	public int getWeightage() {
		return weightage;
	}

	/**
	 * @param weightage the weightage to set
	 */
	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}

	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
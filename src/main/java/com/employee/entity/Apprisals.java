package com.employee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "apprisals")
public class Apprisals {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "apprisal_id")
	private int apprisalId;

	@Column(name = "title")
	private String title;

	@JsonIgnore
	@Column(name = "is_active", nullable = false)
	private int isActive;
	
	@Column(name = "is_completed", nullable = false)
	private int isCompleted;
	
	@Column(name = "is_deleted", nullable = false)
	private int isDeleted;

	/**
	 * @return the apprisalId
	 */
	public int getApprisalId() {
		return apprisalId;
	}

	/**
	 * @param apprisalId the apprisalId to set
	 */
	public void setApprisalId(int apprisalId) {
		this.apprisalId = apprisalId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the isCompleted
	 */
	public int getIsCompleted() {
		return isCompleted;
	}

	/**
	 * @param isCompleted the isCompleted to set
	 */
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}

	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
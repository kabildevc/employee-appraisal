package com.employee.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "emp_apprisal_ans")
public class EmployeeApprisalAnswer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ans_id", nullable = false)
	private int ansId;
	
	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "emp_apprisal_ans_id", nullable = false)
	private EmployeeApprisalAnswerGroup employeeApprisalAnswerGroup;
	
	@Column(name = "qus_id", nullable = false)
	private int qusId;
	
	@Column(name = "is_checked", nullable = false)
	private int isChecked;
	
	@Column(name = "justification")
	private String justification;
	
	@Column(name ="employee_weightage")
	private int employeeWeightage;
	
	@Column(name = "reviewer_weightage")
	private int reviewerWeightage;
	
	@Column(name = "appraiser_weightage")
	private int appraiserWeightage;
	
	@Column(name = "remarks")
	private String remarks;

	/**
	 * @return the ansId
	 */
	public int getAnsId() {
		return ansId;
	}

	/**
	 * @param ansId the ansId to set
	 */
	public void setAnsId(int ansId) {
		this.ansId = ansId;
	}

	/**
	 * @return the employeeApprisalAnswerGroup
	 */
	public EmployeeApprisalAnswerGroup getEmployeeApprisalAnswerGroup() {
		return employeeApprisalAnswerGroup;
	}

	/**
	 * @param employeeApprisalAnswerGroup the employeeApprisalAnswerGroup to set
	 */
	public void setEmployeeApprisalAnswerGroup(EmployeeApprisalAnswerGroup employeeApprisalAnswerGroup) {
		this.employeeApprisalAnswerGroup = employeeApprisalAnswerGroup;
	}

	/**
	 * @return the qusId
	 */
	public int getQusId() {
		return qusId;
	}

	/**
	 * @param qusId the qusId to set
	 */
	public void setQusId(int qusId) {
		this.qusId = qusId;
	}

	/**
	 * @return the isChecked
	 */
	public int getIsChecked() {
		return isChecked;
	}

	/**
	 * @param isChecked the isChecked to set
	 */
	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}

	/**
	 * @return the justification
	 */
	public String getJustification() {
		return justification;
	}

	/**
	 * @param justification the justification to set
	 */
	public void setJustification(String justification) {
		this.justification = justification;
	}

	/**
	 * @return the employeeWeightage
	 */
	public int getEmployeeWeightage() {
		return employeeWeightage;
	}

	/**
	 * @param employeeWeightage the employeeWeightage to set
	 */
	public void setEmployeeWeightage(int employeeWeightage) {
		this.employeeWeightage = employeeWeightage;
	}

	/**
	 * @return the reviewerWeightage
	 */
	public int getReviewerWeightage() {
		return reviewerWeightage;
	}

	/**
	 * @param reviewerWeightage the reviewerWeightage to set
	 */
	public void setReviewerWeightage(int reviewerWeightage) {
		this.reviewerWeightage = reviewerWeightage;
	}

	/**
	 * @return the appraiserWeightage
	 */
	public int getAppraiserWeightage() {
		return appraiserWeightage;
	}

	/**
	 * @param appraiserWeightage the appraiserWeightage to set
	 */
	public void setAppraiserWeightage(int appraiserWeightage) {
		this.appraiserWeightage = appraiserWeightage;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}

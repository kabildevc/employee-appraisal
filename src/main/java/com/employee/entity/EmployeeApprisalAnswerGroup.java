package com.employee.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "emp_apprisal_ans_group")
public class EmployeeApprisalAnswerGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "emp_apprisal_ans_id", nullable = false)
	private int empApprisalAnsId;
	
	@Column(name = "emp_id", nullable = false)
	private int empId;
	
	@Column(name = "apprisal_id", nullable = false)
	private int apprisalId;
	
	@Column(name = "notes")
	private String notes;
	
	@Column(name = "appraiser_id", nullable = false)
	private int appraiserId;
	
	@Column(name ="is_submitted", nullable = false)
	private int isSubmitted;
	
	@Column(name = "inserted_date")
	private Date insertedDate;
	
	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "appraiser_updated_date")
	private Date appraiserUpdatedDate;
	
	@OneToMany(mappedBy = "employeeApprisalAnswerGroup",cascade = CascadeType.ALL)
	private List<EmployeeApprisalAnswer> employeeApprisalAnswers;

	/**
	 * @return the empApprisalAnsId
	 */
	public int getEmpApprisalAnsId() {
		return empApprisalAnsId;
	}

	/**
	 * @param empApprisalAnsId the empApprisalAnsId to set
	 */
	public void setEmpApprisalAnsId(int empApprisalAnsId) {
		this.empApprisalAnsId = empApprisalAnsId;
	}

	/**
	 * @return the empId
	 */
	public int getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	/**
	 * @return the apprisalId
	 */
	public int getApprisalId() {
		return apprisalId;
	}

	/**
	 * @param apprisalId the apprisalId to set
	 */
	public void setApprisalId(int apprisalId) {
		this.apprisalId = apprisalId;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the appraiserId
	 */
	public int getAppraiserId() {
		return appraiserId;
	}

	/**
	 * @param appraiserId the appraiserId to set
	 */
	public void setAppraiserId(int appraiserId) {
		this.appraiserId = appraiserId;
	}

	/**
	 * @return the isSubmitted
	 */
	public int getIsSubmitted() {
		return isSubmitted;
	}

	/**
	 * @param isSubmitted the isSubmitted to set
	 */
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	/**
	 * @return the insertedDate
	 */
	public Date getInsertedDate() {
		return insertedDate;
	}

	/**
	 * @param insertedDate the insertedDate to set
	 */
	public void setInsertedDate(Date insertedDate) {
		this.insertedDate = insertedDate;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the appraiserUpdatedDate
	 */
	public Date getAppraiserUpdatedDate() {
		return appraiserUpdatedDate;
	}

	/**
	 * @param appraiserUpdatedDate the appraiserUpdatedDate to set
	 */
	public void setAppraiserUpdatedDate(Date appraiserUpdatedDate) {
		this.appraiserUpdatedDate = appraiserUpdatedDate;
	}

	/**
	 * @return the employeeApprisalAnswers
	 */
	public List<EmployeeApprisalAnswer> getEmployeeApprisalAnswers() {
		return employeeApprisalAnswers;
	}

	/**
	 * @param employeeApprisalAnswers the employeeApprisalAnswers to set
	 */
	public void setEmployeeApprisalAnswers(List<EmployeeApprisalAnswer> employeeApprisalAnswers) {
		this.employeeApprisalAnswers = employeeApprisalAnswers;
	}
	
}

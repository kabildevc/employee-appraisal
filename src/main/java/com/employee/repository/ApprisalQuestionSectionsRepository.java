package com.employee.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.employee.entity.ApprisalQuestionSections;

public interface ApprisalQuestionSectionsRepository extends CrudRepository<ApprisalQuestionSections, Integer> {

	List<ApprisalQuestionSections> findByIsDeleted(int isDelete);

}
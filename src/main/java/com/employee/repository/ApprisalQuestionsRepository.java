package com.employee.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.employee.entity.ApprisalQuestions;

public interface ApprisalQuestionsRepository extends CrudRepository<ApprisalQuestions, Integer> {

	List<ApprisalQuestions> findByQusSectionIdAndIsDeleted(int qusSectionId, int isDelete);

}
package com.employee.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.employee.entity.Apprisals;

public interface ApprisalsRepository extends CrudRepository<Apprisals, Integer> {

	List<Apprisals> findByIsActiveAndIsCompletedAndIsDeleted(int isActive, int isCompleted, int isDeleted);

}
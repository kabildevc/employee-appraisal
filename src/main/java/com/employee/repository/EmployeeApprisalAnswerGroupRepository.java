package com.employee.repository;

import org.springframework.data.repository.CrudRepository;

import com.employee.entity.EmployeeApprisalAnswerGroup;

public interface EmployeeApprisalAnswerGroupRepository extends CrudRepository<EmployeeApprisalAnswerGroup, Integer>{

	EmployeeApprisalAnswerGroup findByEmpId(int empId);

	EmployeeApprisalAnswerGroup findByEmpApprisalAnsId(int empApprisalAnsId);

	EmployeeApprisalAnswerGroup findByEmpIdAndIsSubmitted(int empId, int isSubmitted);

}

package com.employee.repository;

import org.springframework.data.repository.CrudRepository;

import com.employee.entity.EmployeeApprisalAnswer;

public interface EmployeeApprisalAnswerRepository extends CrudRepository<EmployeeApprisalAnswer, Integer>{

	EmployeeApprisalAnswer findByEmployeeApprisalAnswerGroupEmpIdAndQusId(int empId, int qusId);

	int deleteByEmployeeApprisalAnswerGroupEmpApprisalAnsId(int empApprisalAnsId);

	EmployeeApprisalAnswer findByAnsId(int answerId);

}

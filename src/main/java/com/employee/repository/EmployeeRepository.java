package com.employee.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.employee.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{

	Employee findByEmpCode(String empCode);

	Employee findByEmpId(int empId);
	
	@Query(value = "SELECT UTC_TIMESTAMP()" , nativeQuery=true)
	Date getUtcCurrentDate();
}

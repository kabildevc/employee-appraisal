package com.employee.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.employee.entity.ApprisalQuestionSections;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeApprisalAnswer;
import com.employee.repository.ApprisalQuestionSectionsRepository;
import com.employee.repository.EmployeeApprisalAnswerGroupRepository;
import com.employee.repository.EmployeeApprisalAnswerRepository;
import com.employee.repository.EmployeeRepository;

@Service
@Transactional
public class AdminService {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	ApprisalQuestionSectionsRepository apprisalQuestionSectionsRepository;
	
	@Autowired
	EmployeeApprisalAnswerGroupRepository employeeApprisalAnswerGroupRepository;
	
	@Autowired
	EmployeeApprisalAnswerRepository employeeApprisalAnswerRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public Employee getEmployeeById(int empId) {
		return employeeRepository.findByEmpId(empId);
	}

	public List<Map<String, Object>> getSubmitedForms() {
		String sql = "SELECT emp_apprisal_ans_group.emp_apprisal_ans_id as answerGroupId, "
				+"emp_apprisal_ans_group.emp_id as empId, "
				+"DATE_FORMAT(CONVERT_TZ(emp_apprisal_ans_group.inserted_date,'+00:00','+05:30'), '%d-%b-%Y %h:%i %p') as insertedDate, "
				+"DATE_FORMAT(CONVERT_TZ(emp_apprisal_ans_group.updated_date,'+00:00','+05:30'), '%d-%b-%Y %h:%i %p') as updatedDate, "
				+"employee.emp_code as empCode, "
				+"employee.first_name as firstName, "
				+"employee.last_name as lastName, "
				+"employee.designation as designation, "
				+"employee.team as team, "
				+"e1.first_name as appraiserFirstName, e1.last_name as appraiserLastName "
				+"FROM emp_apprisal_ans_group " 
				+" INNER JOIN employee ON emp_apprisal_ans_group.emp_id = employee.emp_id " 
				+"LEFT OUTER JOIN employee e1 ON emp_apprisal_ans_group.appraiser_id = e1.emp_id " 
				+"WHERE emp_apprisal_ans_group.is_submitted = 1";
		return jdbcTemplate.queryForList(sql);
	}
	
	public List<ApprisalQuestionSections> getQuestionSection() {
		return apprisalQuestionSectionsRepository.findByIsDeleted(0);
	}

//	public EmployeeApprisalAnswerGroup getAnswerGroupByEmpId(int empId) {
//		return employeeApprisalAnswerGroupRepository.findByEmpId(empId);
//	}
//
//	public EmployeeApprisalAnswer getAppraiselAnswerByempApprisalAnsId(int empApprisalAnsId) {
//		return employeeApprisalAnswerRepository.findByEmpApprisalAnsId(empApprisalAnsId);
//	}

	/*public List<Map<String, Object>> getQuestions() {
		String sql = "SELECT apprisal_question_sections.section_title,apprisal_question_sections.qus_section_id,apprisal_questions.qus_id,apprisal_questions.question,apprisal_questions.weightage "
				+ "FROM apprisal_question_sections INNER JOIN apprisal_questions ON apprisal_question_sections.qus_section_id = apprisal_questions.qus_section_id WHERE apprisal_question_sections.is_deleted = 0 AND apprisal_questions.is_deleted = 0";
		return jdbcTemplate.queryForList(sql);
	}
	
	public List<Map<String, Object>> getAnswers(int empId) {
		String sql = "SELECT emp_apprisal_ans_group.emp_apprisal_ans_id,emp_apprisal_ans_group.emp_id,emp_apprisal_ans.ans_id,emp_apprisal_ans.qus_id,emp_apprisal_ans.is_checked,emp_apprisal_ans.justification, "
					+ "emp_apprisal_ans.employee_weightage,emp_apprisal_ans.reviewer_weightage,emp_apprisal_ans.appraiser_weightage,emp_apprisal_ans.remarks "
					+ "FROM emp_apprisal_ans_group LEFT JOIN emp_apprisal_ans ON emp_apprisal_ans_group.emp_apprisal_ans_id = emp_apprisal_ans.emp_apprisal_ans_id WHERE emp_apprisal_ans_group.emp_id = "+empId;
		return jdbcTemplate.queryForList(sql);
	}*/
	
	
	public List<Map<String, Object>> getQuestion()
	{
		String sql = "SELECT apprisal_question_sections.qus_section_id as sectionId, "
				+"apprisal_questions.qus_id as qus_id, "
				+"apprisal_questions.question as question, "
				+"apprisal_questions.weightage as weightage "
				+"FROM apprisal_question_sections "
				+"LEFT JOIN apprisal_questions ON apprisal_question_sections.qus_section_id = apprisal_questions.qus_section_id AND apprisal_questions.is_deleted = 0 "
				+"WHERE apprisal_question_sections.is_deleted = 0";
		return jdbcTemplate.queryForList(sql);
	}
	
	public List<Map<String, Object>> getEmployeeForm(int empId){
//		String sql = "SELECT apprisal_questions.question as question, "
//				+"apprisal_questions.weightage as weightage, "
//				+"apprisal_questions.qus_section_id as sectionId, "
//				+"emp_apprisal_ans_group.emp_apprisal_ans_id as emp_apprisal_ans_id, "
//				+"emp_apprisal_ans.qus_id as qus_id, "
//				+"emp_apprisal_ans.ans_id as ans_id, "
//				+"emp_apprisal_ans.is_checked as is_checked, "
//				+"emp_apprisal_ans.justification as justification, "
//				+"emp_apprisal_ans.employee_weightage as employee_weightage, "
//				+"emp_apprisal_ans.reviewer_weightage as reviewer_weightage, "
//				+"emp_apprisal_ans.appraiser_weightage as appraiser_weightage, "
//				+"emp_apprisal_ans.remarks as remarks "
//				+"FROM emp_apprisal_ans_group  "
//				+"LEFT JOIN emp_apprisal_ans ON emp_apprisal_ans_group.emp_apprisal_ans_id = emp_apprisal_ans.emp_apprisal_ans_id " 
//				+"LEFT JOIN apprisal_questions ON emp_apprisal_ans.qus_id = apprisal_questions.qus_id AND apprisal_questions.is_deleted = 0 "
//				+"WHERE emp_apprisal_ans_group.emp_id = "+ empId;
		
		String sql = "SELECT apprisal_questions.question as question, "
				+"apprisal_questions.weightage as weightage, "
				+"apprisal_question_sections.qus_section_id as sectionId, "
				+"emp_apprisal_ans_group.emp_apprisal_ans_id as emp_apprisal_ans_id, "
				+"emp_apprisal_ans.qus_id as qus_id, "
				+"emp_apprisal_ans.ans_id as ans_id, "
				+"emp_apprisal_ans.is_checked as is_checked, "
				+"emp_apprisal_ans.justification as justification, "
				+"emp_apprisal_ans.employee_weightage as employee_weightage, "
				+"emp_apprisal_ans.reviewer_weightage as reviewer_weightage, "
				+"emp_apprisal_ans.appraiser_weightage as appraiser_weightage, "
				+"emp_apprisal_ans.remarks as remarks "
				+"FROM apprisal_question_sections "
				+"LEFT JOIN apprisal_questions ON apprisal_question_sections.qus_section_id = apprisal_questions.qus_section_id AND apprisal_questions.is_deleted = 0 "
				+"LEFT OUTER JOIN emp_apprisal_ans ON apprisal_questions.qus_id = emp_apprisal_ans.qus_id "
				+"LEFT OUTER JOIN emp_apprisal_ans_group ON emp_apprisal_ans.emp_apprisal_ans_id = emp_apprisal_ans_group.emp_apprisal_ans_id AND emp_apprisal_ans_group.emp_id = "+empId
				+" WHERE apprisal_question_sections.is_deleted = 0";
		return jdbcTemplate.queryForList(sql);
	}

	public EmployeeApprisalAnswer getEmployeeAnswerById(int answerId) {
		return employeeApprisalAnswerRepository.findByAnsId(answerId);
	}

	public EmployeeApprisalAnswer saveEmployeeApprisalAnswer(EmployeeApprisalAnswer employeeApprisalAnswer) {
		return employeeApprisalAnswerRepository.save(employeeApprisalAnswer);
	}
}

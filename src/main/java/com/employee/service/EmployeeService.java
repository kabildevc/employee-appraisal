package com.employee.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.entity.ApprisalQuestionSections;
import com.employee.entity.ApprisalQuestions;
import com.employee.entity.Apprisals;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeApprisalAnswer;
import com.employee.entity.EmployeeApprisalAnswerGroup;
import com.employee.repository.ApprisalQuestionSectionsRepository;
import com.employee.repository.ApprisalQuestionsRepository;
import com.employee.repository.ApprisalsRepository;
import com.employee.repository.EmployeeApprisalAnswerGroupRepository;
import com.employee.repository.EmployeeApprisalAnswerRepository;
import com.employee.repository.EmployeeRepository;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	ApprisalQuestionSectionsRepository apprisalQuestionSectionsRepository;
	
	@Autowired
	ApprisalQuestionsRepository apprisalQuestionsRepository;
	
	@Autowired
	EmployeeApprisalAnswerGroupRepository employeeApprisalAnswerGroupRepository;
	
	@Autowired
	ApprisalsRepository apprisalsRepository;
	
	@Autowired
	EmployeeApprisalAnswerRepository employeeApprisalAnswerRepository;
	
	public Date getUtcTime() {
		return employeeRepository.getUtcCurrentDate();
	}
	
	public Employee getEmployeeByEmpCode(String empCode) {
		return employeeRepository.findByEmpCode(empCode);
	}

	public Employee getEmployeeById(int empId) {
		return employeeRepository.findByEmpId(empId);
	}

	public Employee saveEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public List<ApprisalQuestionSections> getApprisalQuestionSections(int isDelete) {
		return apprisalQuestionSectionsRepository.findByIsDeleted(isDelete);
	}

	public List<ApprisalQuestions> getApprisalQuestions(int qusSectionId,int isDelete) {
		return apprisalQuestionsRepository.findByQusSectionIdAndIsDeleted(qusSectionId,isDelete);
	}

	public EmployeeApprisalAnswerGroup saveEmployeeApprisalAnswerGroup(EmployeeApprisalAnswerGroup apprisalQuestions) {
		return employeeApprisalAnswerGroupRepository.save(apprisalQuestions);
	}

	public EmployeeApprisalAnswer saveEmployeeApprisalAnswer(EmployeeApprisalAnswer employeeApprisalAnswer) {
		return employeeApprisalAnswerRepository.save(employeeApprisalAnswer);
	}

	public List<Apprisals> getApprisals(int isActive, int isCompleted, int isDeleted) {
		return apprisalsRepository.findByIsActiveAndIsCompletedAndIsDeleted(isActive, isCompleted, isDeleted);
	}

	public EmployeeApprisalAnswer getEmployeeApprisalAnswer(int empId, int qusId) {
		return employeeApprisalAnswerRepository.findByEmployeeApprisalAnswerGroupEmpIdAndQusId(empId,qusId);
	}

	public EmployeeApprisalAnswerGroup getEmployeeApprisalAnswerGroupByEmployeeIdAndIsSubmitted(int empId,int isSubmitted) {
		return employeeApprisalAnswerGroupRepository.findByEmpIdAndIsSubmitted(empId,isSubmitted);
	}

	public int deleteByEmpApprisalAnsId(int empApprisalAnsId) {
		return employeeApprisalAnswerRepository.deleteByEmployeeApprisalAnswerGroupEmpApprisalAnsId(empApprisalAnsId);
		
	}

	public EmployeeApprisalAnswerGroup getEmployeeApprisalAnswerGroupById(int empApprisalAnsId) {
		return employeeApprisalAnswerGroupRepository.findByEmpApprisalAnsId(empApprisalAnsId);
		
	}

	public EmployeeApprisalAnswerGroup getEmployeeApprisalAnswerGroupByEmployeeId(int empId) {
		return employeeApprisalAnswerGroupRepository.findByEmpId(empId);
	}
}

package com.employee.util;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Random;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.employee.entity.Employee;

@Component
public class Util {

	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

	public static String generateToken() {
		Random random = new SecureRandom();
		String token = new BigInteger(70, random).toString(32);
		return token;
	}

	public static String passwordEncryption(String Password) {
		String encryptedPassword = null;
		try {
			encryptedPassword = calculateRFC2104HMACSHA256(Password, "Hm3@U*/a6Z");
		} catch (Exception e) {

		}
		return encryptedPassword;
	}

	private static String toHexString(byte[] bytes) {
		@SuppressWarnings("resource")
		Formatter formatter = new Formatter();
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}

	public static String calculateRFC2104HMACSHA256(String data, String key)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(data.getBytes()));
	}

	public ModelAndView getSessionValue(HttpSession session) {
		Employee employee = (Employee) session.getAttribute("employee");
		ModelAndView model = null;
		if(employee == null){
			model = new ModelAndView("redirect:/employee/login");
			session.setAttribute("error", "Your session is expired Login Again!");
			return model;
		}
		return model;
	}
	
	public static java.sql.Date getSQLDate(Date date) {
		return new java.sql.Date(date.getTime());
	}
	
	public static String convertUtcToLocalTime(Date date) {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be
		return formatter.format(date);
	}
}

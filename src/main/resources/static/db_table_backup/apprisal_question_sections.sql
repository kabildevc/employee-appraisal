/*
Navicat MySQL Data Transfer

Source Server         : Localhost - MySql
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : employee_performance_appraisal

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2016-12-01 12:49:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `apprisal_question_sections`
-- ----------------------------
DROP TABLE IF EXISTS `apprisal_question_sections`;
CREATE TABLE `apprisal_question_sections` (
  `qus_section_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `section_title` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`qus_section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of apprisal_question_sections
-- ----------------------------
INSERT INTO `apprisal_question_sections` VALUES ('1', 'Customer Orientation', '0');
INSERT INTO `apprisal_question_sections` VALUES ('2', 'Operational Excellence', '0');
INSERT INTO `apprisal_question_sections` VALUES ('3', 'Self-Development', '0');
INSERT INTO `apprisal_question_sections` VALUES ('4', 'Financial Adherence', '0');
INSERT INTO `apprisal_question_sections` VALUES ('5', 'People Management', '0');

/*
Navicat MySQL Data Transfer

Source Server         : Localhost - MySql
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : employee_performance_appraisal

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2016-12-01 12:49:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `apprisal_questions`
-- ----------------------------
DROP TABLE IF EXISTS `apprisal_questions`;
CREATE TABLE `apprisal_questions` (
  `qus_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `qus_section_id` bigint(20) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `weightage` int(5) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`qus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of apprisal_questions
-- ----------------------------
INSERT INTO `apprisal_questions` VALUES ('1', '1', 'Number of customer appreciations / acknowledgement for the solution provided >=', '15', '0');
INSERT INTO `apprisal_questions` VALUES ('2', '1', 'Resolved Issues as % of Total Issues', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('3', '1', 'Number of Projects delivered as per the schedule in %', '15', '0');
INSERT INTO `apprisal_questions` VALUES ('4', '1', 'Number of escalation to the deliverable <=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('5', '1', '% Adherence to Process >=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('6', '2', '% Adherence to coding standards >=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('7', '2', 'Number of Knowledge assets created >=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('8', '2', 'Number of Modules Proficient in =', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('9', '2', '% Adherence to Process >=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('10', '3', 'Number of Certifications obtained >=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('11', '3', 'Number of new technologies learnt >=(in addition to project deliverables)', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('12', '4', 'contribution to revenue growth', '15', '0');
INSERT INTO `apprisal_questions` VALUES ('13', '4', 'Number of defects and iterations <=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('14', '5', 'Number of knowledge sharing sessions / technical training sessions conducted for Team members>=', '10', '0');
INSERT INTO `apprisal_questions` VALUES ('15', '5', 'Number of Associates mentored >=', '10', '0');
